// get all symbol names for search drop down
function getAllSymbols(data,val,limitToFilter)
{
	var symbolList = [];
	angular.forEach(data, (function(
			item) {
		if (item.name.toLowerCase().indexOf(
				val.toLowerCase()) >= 0) {
			symbolList.push(item);
		}
	}));

	return limitToFilter(symbolList, 5);
}

// Check browser back button press
$(window).on('popstate', function() {
	
	// close advance search pop up
	$('#search-popup').modal('hide');

	//close candelstic pop up if it is open
	var expander = hs.getExpander();
	if (expander) {
		hs.close();
	}
});

//reset advance search fields on closing the model
$(window).on('hidden.bs.modal', function(e){
	document.getElementById("form").reset();
	
	// check radio button
	document.getElementById("dateradio").checked = true;
});

