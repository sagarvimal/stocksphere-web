'use strict';

/* App Module */
var ssAppModule = angular.module('ssAppModule', [ 'ngRoute',
		'ssAppControllers', 'angularUtils.directives.dirPagination',
		'ui.bootstrap' ]);

ssAppModule.config([ '$routeProvider', function($routeProvider) {
	$routeProvider.when('/', {
		templateUrl : 'home.html',
		controller : 'homeController'
	}).when('/see-all/:operation', {
		templateUrl : 'see-all.html',
		controller : 'seeAllController'
	}).when('/show-details/:symbol', {
		templateUrl : 'show-details.html',
		controller : 'showDetailsController'
	}).when('/advance-search', {
		redirectTo : '/see-all/getSearchResults'
	}).otherwise({
		redirectTo : '/'
	});
} ]);
// end module

/* service */

ssAppModule.constant('RESOURCE', (function() {
	
	 // var resource = 'http://localhost:8080';
	var resource = 'http://stocksphere.in';
	  return {
	    DOMAIN: resource,
	    API: resource + '/stocksphere-service/services/get/'
	//    API: resource + '/SSAppServices/services/get/'
	  }
	})());

ssAppModule.constant('APPCONSTANTS', (function() {
	
	  return {
	    DATE_FOTMAT: 'dd/MM/yyyy',
	    MIN_DATE: '2015/01/01',
	    MAX_RESULT_COUNT:'2000',
	    DEFAULT_NO_OF_DAYS:'5'
	  }
	})());

ssAppModule.service("ssAppDataService", function() {

	var data = {};

	return {
		getData : function() {
			return data;
		},
		setData : function(newData) {
			data = newData;
		}
	};
})
// end service

/* Pagination */
function PaginationController($scope) {
	$scope.pageChangeHandler = function(num) {
		console.log('going to page ' + num);
	};
}

ssAppModule.directive('analytics', ['$rootScope', '$location',
                            function ($rootScope, $location) {
                            return {
                                link: function (scope, elem, attrs, ctrl) {
                                    $rootScope.$on('$routeChangeSuccess', function(event, currRoute, prevRoute) {
                                        ga('set', 'page', $location.path());
                                        ga('send', 'pageview');
                                    });
                                }
                            }
                        }]);
// end directive

/* Type head controller */
function TypeaheadController($scope, $http, limitToFilter,RESOURCE) {

	$scope.getSymbols = function(val) {

		var url = RESOURCE.API+'allScriptNames';

		// get data from local amplify store
		var data = amplify.store(url);

		if (data != null) {

			console.log("Getting data from Local store");
			return getAllSymbols(data, val, limitToFilter);

		} else {
			return $http.get(url, {
				params : {
					name : val,
					sensor : false
				}
			}).then(
					function(response) {

						console.log("Getting data from server");

						// store data locally
						amplify.store(url, response.data.stock_data, {
							expires : 600000
						});

						return getAllSymbols(response.data.stock_data, val,
								limitToFilter);
					});
		}
	};
}

// end

/* Controllers */

var ssAppControllers = angular.module('ssAppControllers', []);

ssAppControllers
		.controller(
				'seeAllController',
				[
						'$scope',
						'$http',
						'$routeParams',
						'$filter',
						'$location',
						'ssAppDataService',
						'$anchorScroll',
						'RESOURCE',
						'APPCONSTANTS',
						function($scope, $http, $routeParams, $filter,
								$location, ssAppDataService, $anchorScroll,RESOURCE,APPCONSTANTS) {
							var operation = $routeParams.operation;

							if (operation == 'getDetails') {
								$http(
										{
											method : 'GET',
											url : RESOURCE.API+'performingStocks?count='+APPCONSTANTS.MAX_RESULT_COUNT+'&numberOfDays='+APPCONSTANTS.DEFAULT_NO_OF_DAYS,
											cache : true
										})
										.success(
												function(data) {

													$scope.stockdata = [];

													if (data && data.stock_data) {
														$scope.stock_data_count = data.stock_data.length;
														$scope.show_data = true;
														$scope.stock_data_positive_count = 0;
														$scope.stock_data_negative_count = 0;

														$scope.start_date = data.stock_data[0].startDate;
														$scope.end_date = data.stock_data[0].endDate;

														for (var i = 0; i < data.stock_data.length; i++) {
															$scope.stockdata
																	.push({
																		symbol : data.stock_data[i].symbol,
																		name : data.stock_data[i].name,
																		percentage : parseFloat(data.stock_data[i].percentage),
																		closingPriceDiff : parseFloat(data.stock_data[i].closingPriceDiff),
																		startClosingPrice : parseFloat(data.stock_data[i].startClosingPrice),//latest closing price
																		averageVolume : parseFloat(data.stock_data[i].averageVolume),
																		lowPrice : parseFloat(data.stock_data[i].lowPrice),
																		highPrice : parseFloat(data.stock_data[i].highPrice),
																		twitterSymbol : data.stock_data[i].twitterSymbol
																	});
															if (parseFloat(data.stock_data[i].closingPriceDiff) > 0) {
																$scope.stock_data_positive_count++;
															} else {
																$scope.stock_data_negative_count++;
															}
														}
													} else {
														// show message when
														// there is no data
														// received from server
														$scope.stockdata = [ "no data" ];
														$scope.show_data = false;
														$scope.no_data_found_msg = "Your search did not match any data. Make sure your search critarias are correct.";
													}
												})
										.error(
												function() {
													console
															.log("Error in seeAllController");
													
													//tracking code
													 ga('send', 'exception', {
													      'exDescription':'Error in fetching data of performingStocks for given number of days' 
													    });
												});

								// default sort with decreasing order of
								// percentage
								$scope.predicate = 'percentage';
								$scope.reverse = true;
								$scope.zero = 0;

								// pagination
								$scope.currentPage = 1;
								$scope.pageSize = 50;
								$scope.pageChangeHandler = function(num) {
									console.log('going to page ' + num);
								};
							} else if (operation == 'getSearchResults') {

								$('#search-popup').modal('hide');

								if (ssAppDataService
										&& ssAppDataService.getData()) {
									var datedaysradio = ssAppDataService
											.getData().datedaysradio;

									console.log('datedaysradio : '
											+ datedaysradio);

									if (datedaysradio == 'date') {
										var dateminrange = ssAppDataService
												.getData().dateminrange;
										var datemaxrange = ssAppDataService
												.getData().datemaxrange;
									} else if (datedaysradio == 'days') {
										var numberofdays = ssAppDataService
												.getData().numberofdays;
									}

									var priceminrange = ssAppDataService
											.getData().priceminrange;
									var pricemaxrange = ssAppDataService
											.getData().pricemaxrange;
								}

								// if all fields are undefined then redirect to
								// home page
								if (priceminrange == undefined
										&& pricemaxrange == undefined
										&& datemaxrange == undefined
										&& dateminrange == undefined
										&& numberofdays == undefined) {
									$location.path("/");
								}

								// Convert dates to required format dd/MM/yyyy
								dateminrange = $filter('date')(dateminrange,
										APPCONSTANTS.DATE_FOTMAT);
								datemaxrange = $filter('date')(datemaxrange,
										APPCONSTANTS.DATE_FOTMAT);
								
								if(numberofdays == 1)
									{
										$scope.show_single_day_result = true;
									}	
									
								console.log("priceminrange: " + priceminrange
										+ "  pricemaxrange: " + pricemaxrange
										+ "  dateminrange: " + dateminrange
										+ "  datemaxrange: " + datemaxrange
										+ "  numberofdays: " + numberofdays);

								$http(
										{
											method : 'GET',
											url : RESOURCE.API+'performingStocks?fromDate='
													+ dateminrange
													+ '&toDate='
													+ datemaxrange
													+ '&numberOfDays='
													+ numberofdays
													+ '&count=2000&minRange='
													+ priceminrange
													+ '&maxRange='
													+ pricemaxrange,
											cache : true
										})
										.success(
												function(data) {

													$scope.stockdata = [];

													if (data && data.stock_data) {
														$scope.stock_data_count = data.stock_data.length;
														$scope.show_data = true;
														$scope.stock_data_positive_count = 0;
														$scope.stock_data_negative_count = 0;

														$scope.start_date = data.stock_data[0].startDate;
														$scope.end_date = data.stock_data[0].endDate;

														for (var i = 0; i < data.stock_data.length; i++) {
															$scope.stockdata
																	.push({
																		symbol : data.stock_data[i].symbol,
																		name : data.stock_data[i].name,
																		percentage : parseFloat(data.stock_data[i].percentage),
																		closingPriceDiff : parseFloat(data.stock_data[i].closingPriceDiff),
																		startClosingPrice : parseFloat(data.stock_data[i].startClosingPrice),//latest closing price
																		averageVolume : parseFloat(data.stock_data[i].averageVolume),
																		lowPrice : parseFloat(data.stock_data[i].lowPrice),
																		highPrice : parseFloat(data.stock_data[i].highPrice),
																		twitterSymbol : data.stock_data[i].twitterSymbol
																	});
															if (parseFloat(data.stock_data[i].closingPriceDiff) > 0) {
																$scope.stock_data_positive_count++;
															} else {
																$scope.stock_data_negative_count++;
															}
														}
													} else {
														// show message when
														// there is no data
														// received from server
														$scope.stockdata = [ "no data" ];
														$scope.show_data = false;
														$scope.no_data_found_msg = "Your search did not match any data. Make sure your search critaria is correct.";
														
														//tracking code
														 ga('send', 'exception', {
														      'exDescription':'No data found for advance search' 
														    });
													}

												})
										.error(
												function() {
													console
															.log("Error in seeAllController");
													
													//tracking code
													 ga('send', 'exception', {
													      'exDescription':'Error in fetching data of performingStocks for advance search' 
													    });
												});

								// default sort with decreasing order of
								// percentage
								$scope.predicate = 'percentage';
								$scope.reverse = true;
								$scope.zero = 0;

								// pagination
								$scope.currentPage = 1;
								$scope.pageSize = 50;
								$scope.pageChangeHandler = function(num) {
									console.log('going to page ' + num);
								};

							} else {
								console
										.log('inside else section of seeAllController');
							}
							$anchorScroll();
						} ]);

ssAppControllers.controller('homeController', [ '$scope', '$anchorScroll',
		function($scope, $anchorScroll) {

			$anchorScroll();
			// load home page bar chart
			$(function() {
				showBarChart();
			});
		} ]);

ssAppControllers
		.controller(
				'showDetailsController',
				[
						'$http',
						'$scope',
						'$routeParams',
						'$anchorScroll',
						'RESOURCE',
						function($http, $scope, $routeParams, $anchorScroll,RESOURCE) {
							var symbol = $routeParams.symbol;

							$scope.symbol = symbol;

							$http(
									{
										method : 'GET',
										url : RESOURCE.API+'allDetailsForScript?symbol='
												+ symbol,
										cache : true
									})
									.success(
											function(data) {

												if (data && data.stock_data) {

													$scope.stockdata = [];

													// show candelstic chart
													showCandelsticChart(
															'show-details-container',
															symbol, false);

													console.log("Succes");
													// Assign data to scope
													// variables
													$scope.stockdata.name = data.stock_data[0].name;
													$scope.stockdata.symbol = data.stock_data[0].symbol;
													$scope.stockdata.date = data.stock_data[0].date;
													$scope.stockdata.open = data.stock_data[0].open;
													$scope.stockdata.close = data.stock_data[0].close;
													$scope.stockdata.previousClose = data.stock_data[0].previousClose;
													$scope.stockdata.high = data.stock_data[0].high;
													$scope.stockdata.low = data.stock_data[0].low;
													$scope.stockdata.changeInPrice = data.stock_data[0].changeInPrice;
													$scope.stockdata.percentageChangeInPrice = data.stock_data[0].percentageChangeInPrice;
													$scope.stockdata.volume = data.stock_data[0].volume;
													
													$scope.stockdata.listingDate = data.stock_data[0].listingDate;
													$scope.stockdata.faceValue = data.stock_data[0].faceValue;
													$scope.stockdata.industry = data.stock_data[0].industry;
													$scope.stockdata.isin = data.stock_data[0].isin;
													$scope.stockdata.issuedCapInShares = data.stock_data[0].issuedCapInShares;
													$scope.stockdata.marketCapCrInr = data.stock_data[0].marketCapCrInr;
													$scope.stockdata.high52Week = data.stock_data[0].high52Week;
													$scope.stockdata.low52Week = data.stock_data[0].low52Week;
													$scope.stockdata.twitterSymbol = data.stock_data[0].twitterSymbol
												} else {

													console
															.log("No data recieved from server for symbol : "
																	+ symbol);
													
													//tracking code
													 ga('send', 'exception', {
													      'exDescription':'Error in fetching details of '+symbol 
													    });
												}
											})
									.error(
											function() {
												console
														.log("Error in showDetailsController");
											});

							$anchorScroll();
						} ]);

ssAppControllers
		.controller(
				'AdvanceSearchController',
				[
						'$scope',
						'$routeParams',
						'$location',
						'$filter',
						'ssAppDataService',
						function($scope, $routeParams, $location, $filter,
								ssAppDataService) {

							console
									.log('inside AdvanceSearchController controller');

							$scope.submit = function(event) {

								// prevent defaults form submission
								event.preventDefault();

								console
										.log('inside submit QuoteSearchController');

								if ($scope.form.$valid) {
									console.log('advance search from is valid');

									//checking for range condition
									var datemin = $scope.advanceSearchForm.dateminrange;
									var datemax = $scope.advanceSearchForm.datemaxrange;
									var pricemin = $scope.advanceSearchForm.priceminrange;
									var pricemax = $scope.advanceSearchForm.pricemaxrange;
									var numberofdays = $scope.advanceSearchForm.numberofdays;

									if ($scope.advanceSearchForm.datedaysradio == 'date'
											&& Date.parse(datemax) <= Date
													.parse(datemin)) {

										$scope.formerror = "Max date should be greater than min date";
										return;
									}
									
									if ($scope.advanceSearchForm.datedaysradio == 'days' && !(numberofdays === parseInt(numberofdays, 10))) {

									$scope.formerror = "Enter a valid whole number as number of days";
									return;
									}

									if (pricemax <= pricemin) {

										$scope.formerror = "Max price should be greater than min price";
										return;
									}

									// setting data from scope to service for
									// use in in different controller
									ssAppDataService
											.setData($scope.advanceSearchForm);
									
									if ($scope.advanceSearchForm.datedaysradio == 'date')
										{
											//tracking code
											ga('send', 'event', 'search', 'advance search - date');
										}else
										{
											//tracking code
											ga('send', 'event', 'search', 'advance search - nod');
										}

									$location.path("/advance-search");
								}
							};
						} ]);

ssAppControllers.controller('QuoteSearchController', [ '$scope',
		'$routeParams', '$location', function($scope, $routeParams, $location) {

			console.log('inside QuoteSearchController controller');

			$scope.submit = function(event) {

				// prevent defaults form submission
				event.preventDefault();

				console.log('inside submit QuoteSearchController');

				if ($scope.quotesearchform.$valid) {
					
					//tracking code
					ga('send', 'event', 'search', 'quote search');
					
					$location.path("/show-details/" + $scope.script.symbol);
					$scope.script = '';
				}
			};
		} ]);

ssAppControllers.controller('MaxDatepickerCtrl',
		function($scope) {

			$scope.today = function() {
				$scope.datemaxrange = new Date();
			};
			$scope.today();

			$scope.clear = function() {
				$scope.datemaxrange = null;
			};

			// Disable weekend selection
			$scope.disabled = function(date, mode) {
				return (mode === 'day' && (date > new Date() || date < Date
						.parse('2015/01/01')));
			};

			$scope.open = function($event) {
				$event.preventDefault();
				$event.stopPropagation();

				$scope.opened = true;
			};

			$scope.dateOptions = {
				formatYear : 'yy',
				startingDay : 1
			};

			$scope.formats = [ 'dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd/MM/yyyy',
					'shortDate' ];
			$scope.format = $scope.formats[2];
		});

ssAppControllers.controller('MinDatepickerCtrl',
		function($scope) {

			$scope.today = function() {
				$scope.dateminrange = new Date();
			};
			$scope.today();

			$scope.clear = function() {
				$scope.dateminrange = null;
			};

			// Disable weekend selection
			$scope.disabled = function(date, mode) {
				return (mode === 'day' && (date > new Date() || date < Date
						.parse('2015/01/01')));
			};

			$scope.open = function($event) {
				$event.preventDefault();
				$event.stopPropagation();

				$scope.opened = true;
			};

			$scope.dateOptions = {
				formatYear : 'yy',
				startingDay : 1
			};

			$scope.formats = [ 'dd-MMMM-yyyy', 'yyyy-MM-dd', 'dd/MM/yyyy',
					'shortDate' ];
			$scope.format = $scope.formats[2];
		});
// end controllers
