var homePage = {};
homePage.count = 0;
homePage.symbolArray = [];

// highchart global settings
Highcharts.setOptions({
	global: {
		timezoneOffset: -5.5 * 60
	}
}); 

homePage.service_domain = 'http://stocksphere.in/stocksphere-service/'
	
// homePage.service_domain = 'http://localhost:8080/SSAppServices/'

function showBarChart($scope)
{
		$.getJSON(homePage.service_domain+'services/get/performingStocks?count=20&numberOfDays=5', function(data) {

		if(data.stock_data)
		{
			console.log(data);

			var processed_json_all = [];
			var processed_json_symbol = [];
			var processed_json_symbol_name_map  = {};

			// Populate series
			for (var i = 0; i < data.stock_data.length; i++){

				var processed_json_1 = [];
				processed_json_1.push(data.stock_data[i].symbol);
				processed_json_1.push(parseFloat(data.stock_data[i].percentage));
				processed_json_all.push(processed_json_1);

				processed_json_symbol.push(data.stock_data[i].symbol);
				processed_json_symbol_name_map[data.stock_data[i].symbol] = data.stock_data[i].name;
			} 
			
			// add to global symbol array
			homePage.symbolArray = processed_json_symbol;

			$('#container1').highcharts({
				chart: {
					type: 'column'
				},
				title: {
					text: 'Top 20 high performing companies of last 5 days'
				},
				xAxis: {
					title: {
						text: 'Companies'
					},
					labels:{
						enabled:false
					},
					categories: processed_json_symbol
				},
				exporting: {
					buttons: {
		                contextButton: {
		                    enabled: false
		                }
					}
				},
				yAxis: {
					title: {
						text: 'Percentage change in share price'
					}
				},
				plotOptions: {
					column: {
						stacking: 'normal',
						pointPadding: 0.2,
						groupPadding: 0.1,
						dataLabels: {
							enabled: true,
							formatter: function() {return this.x},
							inside: false,
							rotation: 315
						}
					},
					series: {
						cursor: 'pointer',
						point: {
							events: {
								click: // start
									function () {
									// open popup for candelstic chart
									openPopupHome(this.x);
								} //end
							}
						}
					}
				},
				legend: {
					backgroundColor: '#FCFFC5'
				},
				tooltip: {
					backgroundColor: '#FCFFC5',
					useHTML: true,
					formatter: function() {
						if(this.y <=0){
							return '<font color=red><b>'+processed_json_symbol_name_map[this.x] +'</b><br><br> Change % : <b>-'+this.y+'</b></font>';
						}
						else{
							return '<font color=green><b>'+processed_json_symbol_name_map[this.x] +'</b><br><br> Change % : <b>+'+this.y+'</b></font>';
						}
					}
				}, 
				series:
					[{
						showInLegend: false, 
						data: processed_json_all
					}] 
			});
		}
		else
		{
			document.getElementById("container1").innerHTML = "<h3>Currently uable to load data. Please try again letter.</h3>";
		}
	});
}

function openPopupHome(symbolIndex)
{
	var counter = ++homePage.count;
	var divId = 'hc-test'+counter;
	
	var expander = hs.getExpander();
	if (!expander) {
		hs.htmlExpand(null, {
			// headingText: symbol,
			maincontentText: '<div id="'+divId+'"></div>',
			width :950,
			height :500,
		});
	}
	else
	{
		console.log('pop up already open');
	}
	
	// show candelstic chart after opening the popup
	hs.Expander.prototype.onAfterExpand = function(){
		
		showCandelsticChart(divId, homePage.symbolArray[symbolIndex],true);
	};
}

function showCandelsticChart(divId, symbol,displayButtons) {
	
	$(function () {
		$.getJSON(homePage.service_domain+'services/get/detailsForScript?symbol='+symbol, function (data) {

			var npData = data.stock_data;

			// split the data set into ohlc and volume
			var ohlc = [],
			volume = [],
			dataLength = npData.length,
			name = npData[0].name;
			
			// do not display name see details page
			if (!displayButtons) {
				name = null;
			}
			
			// set the allowed units for data grouping
			groupingUnits = [[
			                  'week',                         // unit name
			                  [1]                             // allowed multiples
			                  ], [
			                      'month',
			                      [1, 2, 3, 4, 6]
			                      ]],

			                      i = 0;
			for (i; i < dataLength; i += 1) {
				ohlc.push([
				           parseInt(npData[i].date), // the date
				           parseFloat(npData[i].open), // open
				           parseFloat(npData[i].high), // high
				           parseFloat(npData[i].low), // low
				           parseFloat(npData[i].close) // close
				           ]);

				volume.push([
				             parseInt(npData[i].date), // the date
				             parseFloat(npData[i].volume) // the volume
				             ]);
			}

			// create the chart
			$("#"+divId).highcharts('StockChart', {

				rangeSelector: {
					selected: 1
				},
				plotOptions: {
					candlestick: {
						upColor: 'green',
						color: 'red'
					},
				},

				title: {
					text: name
				},
				tooltip: {
						backgroundColor: '#FCFFC5',
						borderColor: 'blue',
					},
					exporting: {
			            buttons:{
			            	contextButton: {
			                    enabled: false
			                },
			            	gotoScriptPageButton : {
			            		enabled: displayButtons,
			            		_titleKey : "Go to Script Page",
								onclick : function() {
									document.location = '#/show-details/'
											+ symbol;
									hs.close();
									return false;
								},
								symbol : "menu"
							}
			            }
			        },
				yAxis: [{
					labels: {
						align: 'right',
						x: -3
					},
					title: {
						text: 'OHLC'
					},
					height: '60%',
					lineWidth: 2
				}, {
					labels: {
						align: 'right',
						x: -3
					},
					title: {
						text: 'Volume'
					},
					top: '65%',
					height: '35%',
					offset: 0,
					lineWidth: 2
				}],

				series: [{
					type: 'candlestick',
					name: 'NSE: ' + symbol,
					data: ohlc,
					dataGrouping: {
						units: groupingUnits
					}
				}, {
					type: 'column',
					name: 'Volume',
					data: volume,
					yAxis: 1,
					dataGrouping: {
						units: groupingUnits
					}
				}]
			});
		});
	});
}
